package main

// this works on Linux

import (
	"os"
	"fmt"
	"os/exec"
)

	func main() {

		if len(os.Args) < 3 || len(os.Args) > 3 {
			fmt.Println("Please enter in `interface` and then new `mac address`.\nUse sudo when running program. " +
				"Will take permission to change MAC address.")
			fmt.Println("Example Usage: \nsudo ./machanger eth0 00:11:22:33:44:55")
		} else {
			dainterface := os.Args[1]
			macAddress := os.Args[2]

			daifconfig(dainterface, macAddress)
		}

}

func daifconfig(dainterface string, macAddress string) {
	cmd := exec.Command("ifconfig", dainterface, "down")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
	fmt.Println("Shutting down wireless interface...")

	cmd2 := exec.Command("ifconfig", dainterface, "hw", "ether", macAddress)
	cmd2.Stdout = os.Stdout
	cmd2.Stderr = os.Stderr
	cmd2.Run()
	fmt.Println("Changing the MAC address...")

	cmd3 := exec.Command("ifconfig", dainterface, "up")
	cmd3.Stdout = os.Stdout
	cmd3.Stderr = os.Stderr
	cmd3.Run()
	fmt.Println("Bringing " + dainterface + " back up...")
	fmt.Println("Done.")
	
	// run ifconfig to validate that the MAC was changed
	cmd4 := exec.Command("ifconfig", dainterface)
	stdout, _ := cmd4.CombinedOutput()

	outputtoparse := string(stdout)
	fmt.Println(outputtoparse)
	
}
